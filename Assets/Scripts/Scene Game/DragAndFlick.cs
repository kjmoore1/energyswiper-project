﻿using UnityEngine;
using System.Collections;

public class DragAndFlick : MonoBehaviour
{
    // gameobject variables
    Vector3 startPosition;
    Vector3 endPosition;
    float startTime;
    float endTime;

    float duration;
    float distance;
    float power;

    private float objDistance = 9;


    void OnMouseDown()
    {

    }


    void OnMouseDrag()
    {
        if (Pause.paused == false) // if game is not paused
        {
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, objDistance);
            Vector3 ObjPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            this.transform.position = ObjPosition;
            this.rigidbody.velocity = Vector3.zero;

            //Store initial values
            startPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, objDistance);
            startTime = Time.time;
        }
    }


    void OnMouseUp()
    {
        if (Pause.paused == false)
        {
            // Get end values
            endPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, objDistance); // save end position
            endTime = Time.time; // save time

            startPosition = Camera.main.ScreenToWorldPoint(startPosition); // translate start position into screen coords
            endPosition = Camera.main.ScreenToWorldPoint(endPosition); // translate end position into screen coords

            duration = endTime - startTime; // The duration of the swipe
            Vector3 dir = endPosition - startPosition; // The direction of the swipe
            distance = dir.magnitude; // The distance of the swipe
            power = distance / duration; // longer or faster swipes give more power

            Vector3 velocity = dir.normalized * power; // take the direction from the swipe, normalise then times power

            this.rigidbody.velocity = velocity; // Add the force in swipe direction

            print(velocity); // print the velocity applied to the object
        }
    }
}