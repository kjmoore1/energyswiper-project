﻿using UnityEngine;
using System.Collections;

public class BadPlaneCollisionDetection : MonoBehaviour {
	
		// GameObjects
		public GameObject TopWall;
		public GameObject LeftWall;
		public GameObject RightWall;
		public GameObject BottomWall;
		
		public GameObject BadPlane;
		
		public AudioClip PointAquired;
		
		// Score values
		public static int ScoreValueGood = 10;
		public static int ScoreValueBad = 20;
		
		public void OnCollisionEnter(Collision BadPlane) // Start Colliding
		{
			if (BadPlane.gameObject.name == "TopWall")
			{
				ScoreManager.score += ScoreValueGood; // add Points
				Destroy(this.gameObject); // Destroy this gameobject
				//print("Touched a wall");
				Camera.main.audio.PlayOneShot(PointAquired); // play point aquired sound
			}
			
			if (BadPlane.gameObject.name == "LeftWall")
			{
				ScoreManager.score += ScoreValueGood; // add Points
				Destroy(this.gameObject); // Destroy this gameobject
				//print("Touched a wall");
				Camera.main.audio.PlayOneShot(PointAquired); // play point aquired sound
			}
			
			if (BadPlane.gameObject.name == "RightWall")
			{
				ScoreManager.score += ScoreValueGood; // add Points
				Destroy(this.gameObject); // Destroy this gameobject
				//print("Touched a wall");
				Camera.main.audio.PlayOneShot(PointAquired); // play point aquired sound
			}
			
			if (BadPlane.gameObject.name == "BottomWall")
			{
				ScoreManager.score -= ScoreValueBad; // take Points
				Destroy(this.gameObject); // Destroy this gameobject
				//print("Touched bottem wall");
			}
		}
		
		
		void Start()
		{
			
		}
		
		
		void Update()
		{
			TopWall = GameObject.Find("TopWall"); // assign gameobjects
			LeftWall = GameObject.Find("LeftWall");
			RightWall = GameObject.Find("RightWall");
			BottomWall = GameObject.Find("BottomWall");
		}
	}