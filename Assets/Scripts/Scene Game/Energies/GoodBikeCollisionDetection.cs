﻿using UnityEngine;
using System.Collections;

public class GoodBikeCollisionDetection : MonoBehaviour {
	
		// GameObjects
		public GameObject TopWall;
		public GameObject LeftWall;
		public GameObject RightWall;
		public GameObject BottomWall;
		
		public GameObject GoodBike;
		
		public AudioClip PointAquired;
		
		// Score values
		public static int ScoreValueGood = 10;
		public static int ScoreValueBad = 20;
				
		public void OnCollisionEnter(Collision GoodBike) // Start Colliding
		{
			if (GoodBike.gameObject.name == "TopWall")
			{
				ScoreManager.score -= ScoreValueBad; // take Points
				Destroy(this.gameObject); // Destroy this gameobject
				//print("touched a wall");
			}
		
			if (GoodBike.gameObject.name == "LeftWall")
			{
				ScoreManager.score -= ScoreValueBad; // take Points
				Destroy(this.gameObject); // Destroy this gameobject
				//print("touched a wall");
			}
		
			if (GoodBike.gameObject.name == "RightWall")
			{
				ScoreManager.score -= ScoreValueBad; // take Points
				Destroy(this.gameObject); // Destroy this gameobject
				//print("touched a wall");
			}
		
			if (GoodBike.gameObject.name == "BottomWall")
			{
				ScoreManager.score += ScoreValueGood; // add Points
				Destroy(this.gameObject); // Destroy this gameobject
				//print("Touched bottem wall");
				Camera.main.audio.PlayOneShot(PointAquired); // play point aquired sound
			}
		}
		
		void Start()
		{
			
		}
		
		
		void Update()
		{
			TopWall = GameObject.Find("TopWall"); // assign gameobjects
			LeftWall = GameObject.Find("LeftWall");
			RightWall = GameObject.Find("RightWall");
			BottomWall = GameObject.Find("BottomWall");
		}
	}