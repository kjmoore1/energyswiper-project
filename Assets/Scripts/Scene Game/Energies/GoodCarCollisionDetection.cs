﻿using UnityEngine;
using System.Collections;

public class GoodCarCollisionDetection : MonoBehaviour {
	
		// GameObjects
		public GameObject TopWall;
		public GameObject LeftWall;
		public GameObject RightWall;
		public GameObject BottomWall;
		
		public GameObject GoodCar;
		
		public AudioClip PointAquired;
		
		// Score values
		public static int ScoreValueGood = 10;
		public static int ScoreValueBad = 20;
		
		
		public void OnCollisionEnter(Collision GoodCar) // Start Colliding
		{
			if (GoodCar.gameObject.name == "TopWall")
			{
				ScoreManager.score -= ScoreValueBad; // take Points
				Destroy(this.gameObject); // Destroy this gameobject
				//print("touched a wall");
			}
		
			if (GoodCar.gameObject.name == "LeftWall")
			{
				ScoreManager.score -= ScoreValueBad; // take Points
				Destroy(this.gameObject); // Destroy this gameobject
				//print("touched a wall");
			}
		
			if (GoodCar.gameObject.name == "RightWall")
			{
				ScoreManager.score -= ScoreValueBad; // take Points
				Destroy(this.gameObject); // Destroy this gameobject
				//print("touched a wall");
			}
		
			if (GoodCar.gameObject.name == "BottomWall")
			{
				ScoreManager.score += ScoreValueGood; // add Points
				Destroy(this.gameObject); // Destroy this gameobject
				//print("Touched bottem wall");
				Camera.main.audio.PlayOneShot(PointAquired); // play point aquired sound
			}
		}
		
		
		void Start()
		{
			
		}
		
		
		void Update()
		{
			TopWall = GameObject.Find("TopWall"); // assign gameobjects
			LeftWall = GameObject.Find("LeftWall");
			RightWall = GameObject.Find("RightWall");
			BottomWall = GameObject.Find("BottomWall");
		}
	}