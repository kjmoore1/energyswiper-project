﻿using UnityEngine;
using System.Collections;

public class EnergySpawner : MonoBehaviour
{
    public Transform spawner;

	public GameObject[] goodObjectList = new GameObject[3];
	public GameObject[] badObjectList = new GameObject[2];

	private float badEnergyTimePassed = 0f; // Stores value of time passed
	private float badEnergySpawnRate = 15f; // The rate the enemy spawn
	private int badNumberOfEnergy = 1; // How many enemies spawn
	private float badSpawnTime = 1f; // Used to control the spawn time

	private float goodEnergyTimePassed = 0f; // Stores value of time passed
	private float goodEnergySpawnRate = 15f; // The rate the enemy spawn
	private int goodNumberOfEnergy = 1; // How many enemies spawn
	private float goodSpawnTime = 1f; // Used to control the spawn time

	private float waveLevelTimer = 0f; // Used to measure time for Wave Level
	private static int levelCounter = 1;

	Canvas WaveCounter;

	float Range; // Used to find a random range

	void BadEnergySpawn ()
	{
		// Store the time gone in variable
		badEnergyTimePassed += Time.deltaTime;
		
		// After 15 seconds
		if (badEnergyTimePassed >= 15f) 
		{
			// Minus '0.3' of spawn time if 'spawntime' isnt '1'
			if (badSpawnTime >= 30) 
			{
				badSpawnTime -= 0.3f;
			}
			
			// Spawn 'x' amount of enemies
			badNumberOfEnergy += 1;
			// Time vaule reset
			badEnergyTimePassed = 0f;
		}

		// Controls the spawnrate, without it, it would just keep spawning
		if (badEnergySpawnRate < Time.time) 
		{
			// Loop is used to initiate spawn
			for (int i = 0; i < badNumberOfEnergy; i++) 
			{
				Vector3 initialForce = new Vector3(Random.Range(-8.0F, 8.0F), Random.Range(40.0F, 60.0F), 0); // random force in x and y
				Vector3 initialTorque = new Vector3(Random.Range(-10.0F, 10.0F), Random.Range(-10.0F, 10.0F), Random.Range(-10.0F, 10.0F)); // random torque
				
				GameObject BadCubeInstance; // assign cubeinstance
				BadCubeInstance = Instantiate( badObjectList[Random.Range (0,1)], spawner.transform.position, spawner.rotation) as GameObject; // instansiate as cube instance
				BadCubeInstance.rigidbody.AddForce(initialForce * 10); // add force
				BadCubeInstance.rigidbody.AddTorque(initialTorque * 10); // add torque
			}
			
			badEnergySpawnRate = Time.time + badSpawnTime;
		}
	}

	void GoodEnergySpawn ()
	{
		// Store the time gone in variable
		goodEnergyTimePassed += Time.deltaTime;
		
		// After 15 seconds
		if (goodEnergyTimePassed >= 15f) 
		{
			// Minus '0.3' of spawn time if 'spawntime' isnt '1'
			if (goodSpawnTime >= 30) 
			{
				goodSpawnTime -= 0.3f;
			}
			
			// Spawn 'x' amount of enemies
			goodNumberOfEnergy += 1;
			// Time vaule reset
			goodEnergyTimePassed = 0f;
		}
		
		// Controls the spawnrate, without it, it would just keep spawning
		if (goodEnergySpawnRate < Time.time) 
		{
			// Loop is used to initiate spawn
			for (int i = 0; i < goodNumberOfEnergy; i++) 
			{
        		Vector3 initialForce = new Vector3(Random.Range(-8.0F, 8.0F), Random.Range(40.0F, 60.0F), 0); // random force in x and y
        		Vector3 initialTorque = new Vector3(Random.Range(-10.0F, 10.0F), Random.Range(-10.0F, 10.0F), Random.Range(-10.0F, 10.0F)); // random torque

        		GameObject GoodCubeInstance; // assign cubeinstance
        		GoodCubeInstance = Instantiate( goodObjectList[Random.Range (0,2)], spawner.transform.position, spawner.rotation) as GameObject; // instansiate as cube instance
        		GoodCubeInstance.rigidbody.AddForce(initialForce * 10); // add force
        		GoodCubeInstance.rigidbody.AddTorque(initialTorque * 10); // add torque
			}
			
			goodEnergySpawnRate = Time.time + goodSpawnTime;
		}
	}

	//public void WaveLevel()
	//{
		// Store the time gone in variable
		//waveLevelTimer += Time.deltaTime;

		//if (waveLevelTimer >= 10f) 
		//{
		//	levelCounter += 1;
		//	WaveCounter.level += levelCounter;
		//}

		//waveLevelTimer = 0f;
		
	//}

    public void Start()
    {

    }

	void Awake()
	{	// Spawn time depends on time from first frame and value of spawn timer
		badEnergySpawnRate = Time.time + badSpawnTime;
		goodEnergySpawnRate = Time.time + goodSpawnTime;
	}

    public void Update()
    {
		GoodEnergySpawn(); // spawn energy
		BadEnergySpawn(); // spawn energy
		GoodEnergySpawn(); // spawn energy
		//WaveLevel ();	
    }

}