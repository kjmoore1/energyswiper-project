﻿using UnityEngine;
using System.Collections;

public class GoodCollisionDetection : MonoBehaviour
{
    public GameObject TopWall;
    public GameObject LeftWall;
    public GameObject RightWall;
    public GameObject BottomWall;

    public GameObject GoodEnergy;

    public AudioClip PointAquired;

    public static int ScoreValueGood = 10;
    public static int ScoreValueBad = 20;


    public void OnCollisionEnter(Collision GoodEnergy) // Start Colliding
    {
        if (GoodEnergy.gameObject.name == "TopWall")
        {
            ScoreManager.score -= ScoreValueBad; // take Points
            Destroy(this.gameObject); // Destroy this gameobject
            //print("touched a wall");
        }

        if (GoodEnergy.gameObject.name == "LeftWall")
        {
            ScoreManager.score -= ScoreValueBad; // take Points
            Destroy(this.gameObject); // Destroy this gameobject
            //print("touched a wall");
        }

        if (GoodEnergy.gameObject.name == "RightWall")
        {
            ScoreManager.score -= ScoreValueBad; // take Points
            Destroy(this.gameObject); // Destroy this gameobject
            //print("touched a wall");
        }

        if (GoodEnergy.gameObject.name == "BottomWall")
        {
            ScoreManager.score += ScoreValueGood; // add Points
            Destroy(this.gameObject); // Destroy this gameobject
            //print("Touched bottem wall");
            Camera.main.audio.PlayOneShot(PointAquired); // play point aquired sound
        }
    }


    void Start()
    {

    }


    void Update()
    {
        TopWall = GameObject.Find("TopWall"); // assign collision boxes
        LeftWall = GameObject.Find("LeftWall");
        RightWall = GameObject.Find("RightWall");
        BottomWall = GameObject.Find("BottomWall");
    }
}