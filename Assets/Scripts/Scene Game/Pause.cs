﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour
{
    public Font GuiFont;

    public static bool paused = false;
    private bool showTexture = false;

    bool btnMenuClick = false;
    bool btnQuitGameClick = false;


    void OnMouseDown()
    {
        paused = !paused; // reverse pause variable

        if (paused == true) // If the game is paused
        {
            Time.timeScale = 0; // Set timescale to 0
            paused = true; // pause the game
            showTexture = true; // display pause texture
        }

        if (paused == false) // If the game isn't paused
        {
            Time.timeScale = 1; // Set timescale to 1
            paused = false; // unpause the game
            showTexture = false; // turn off texture
        }

        if (showTexture == true) // if variable true
        {
            GameObject.Find("PauseGuiTexture").guiTexture.enabled = true; // show the texture
        }
        else // if not then
        {
            GameObject.Find("PauseGuiTexture").guiTexture.enabled = false; // hide the texture
        }
    }


    void OnGUI()
    {
        GUI.skin.font = GuiFont; // Set Font

        GUIStyle style = GUI.skin.GetStyle("Button"); // Set Style
        style.fontSize = (int)(80.0f); // Set Font Size

        if (showTexture == true)
        {
            if (btnMenuClick) // If Button is clicked Change Text Colour
                GUI.color = Color.black;
            else
                GUI.color = Color.white;

            if (GUI.Button(new Rect(Screen.width / 5f, Screen.height / 10.0f, Screen.width / 1.7f, Screen.height / 7), "Menu")) // Create a new button with click functionality
            {
                btnMenuClick = true;
                Application.LoadLevel(0);
            }

            
            if (btnQuitGameClick) // If Button is clicked Change Text Colour
                GUI.color = Color.black;
            else
                GUI.color = Color.white;

            if (GUI.Button(new Rect(Screen.width / 5f, Screen.height / 1.5f, Screen.width / 1.7f, Screen.height / 7), "Quit")) // Create a new button with click functionality
            {
                btnQuitGameClick = true;
                Application.Quit(); // terminate application
            }
        }
    }


    void Start()
    {

    }


    void Update()
    {

    }
}
