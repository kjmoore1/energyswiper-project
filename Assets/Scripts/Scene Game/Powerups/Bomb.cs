﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour 
{
    public GameObject MainCamera;

    public AudioClip BombPowerup;

    float Timer = 1.6f;
    public bool StartTimer = false;


    void OnMouseDown()
    {
        StartTimer = true; // start timer
        MainCamera.audio.PlayOneShot(BombPowerup); // play bomb powerup sound
    }


	// Use this for initialization
	void Start () 
    {
	
	}
	

	// Update is called once per frame
	void Update () 
    {
        if (StartTimer == true) // if timer counting
        {
            Timer -= Time.deltaTime; // count down timer
        }
        
        if (Timer < 0f) // if timer finished
        {
            GameObject[] BadEnergy = GameObject.FindGameObjectsWithTag("BadEnergy"); // find every object with tag badenergy
            foreach (GameObject i in BadEnergy) // for each of those objects
            {
                Destroy(i); // destroy those objects
            }
            StartTimer = false; // stop timer
            Timer = 1.6f; // reset timer
        }
	}
}
