﻿using UnityEngine;
using System.Collections;

public class DoublePoints : MonoBehaviour 
{
    public GameObject MainCamera;

    public AudioClip DoublePointsActivate;
    public AudioClip DoublePointsDeactivate;

    public float DoublePointsCountdown = 10f;
    public bool Countingdown = false;
    public bool audioPlayed = false;


    void OnMouseDown()
    {
        if (audioPlayed == false) // if audio hasen't been played
        {
            MainCamera.audio.PlayOneShot(DoublePointsActivate); // Play activate sound
            audioPlayed = true; // audio has been played
        }

        Countingdown = true; // start countdown
    }


	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (Countingdown == true) // if timer is counting down
        {
            DoublePointsCountdown -= Time.deltaTime; // count down timer
        }

        if (DoublePointsCountdown > 0 && DoublePointsCountdown < 10) // Counting Down
        {
            GoodCollisionDetection.ScoreValueBad = 20; // change score values
            GoodCollisionDetection.ScoreValueGood = 20;
            BadCollisionDetection.ScoreValueBad = 20;
            BadCollisionDetection.ScoreValueGood = 20;
        }
        else if (DoublePointsCountdown < 0) // Timer finished
        {
            Countingdown = false;
            DoublePointsCountdown = 10f;
            GoodCollisionDetection.ScoreValueBad = 20; // change score values back
            GoodCollisionDetection.ScoreValueGood = 10;
            BadCollisionDetection.ScoreValueBad = 20;
            BadCollisionDetection.ScoreValueGood = 10;
            MainCamera.audio.PlayOneShot(DoublePointsDeactivate); // play deactivate sound
            audioPlayed = false; // audio has been played
        }
	}
}
