﻿using UnityEngine;
using System.Collections;

public class SlowTime : MonoBehaviour
{
    public GameObject MainCamera;

    public AudioClip SlowTimeActivate;
    public AudioClip SlowTimeDeactivate;

    public float SlowTimeCountdown = 5f;
    public bool Countingdown = false;
    public bool audioPlayed = false;


    void OnMouseDown()
    {
        if (audioPlayed == false) // if audio hasen't been played
        {
            MainCamera.audio.PlayOneShot(SlowTimeActivate); // play slow time activate sound
            audioPlayed = true; // audio now played
        }

        Countingdown = true; // start timer
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Countingdown == true) // timer counting down
        {
            SlowTimeCountdown -= Time.deltaTime; // timer counting down
        }

        if (SlowTimeCountdown > 0 && SlowTimeCountdown < 5) // Counting Down
        {
            if (Time.timeScale == 1.0f) // if timescale is normal
            {
                Time.timeScale = 0.5f; // set timescale to half
            }
        }
        else if (SlowTimeCountdown < 0) // if timer finished
        {
            Countingdown = false; // turn timer off
            MainCamera.audio.PlayOneShot(SlowTimeDeactivate); // play deactivate sound
            SlowTimeCountdown = 5f; // reset timer
            audioPlayed = false; // audio hasen't been played
            Time.timeScale = 1f; // set timescale back to normal
        }
    }
}
