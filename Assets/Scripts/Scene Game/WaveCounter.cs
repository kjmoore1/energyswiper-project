﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WaveCounter : MonoBehaviour {


	public static int level;        // The player's score.
		
		
		Text text;                      // Reference to the Text component.
		
		
		void Awake ()
		{
			// Set up the reference.
			text = GetComponent <Text> ();
			
			// Reset the score.
			level = 1;
		}
		
		
		void Update ()
		{
			// Set the displayed text to be the word "Score" followed by the score value.
			text.text = "Level: " + level;
		}
	}