﻿using UnityEngine;
using System.Collections;

public class AnimationHandler : MonoBehaviour
{
    // Button GameObjects
    public GameObject StartGameBtn;
    public GameObject AboutBtn;
    public GameObject TutorialBtn;

    // Variables
    public bool AnimationPlayed = false;


	// Use this for initialization
	void Start ()
    {
        StartGameBtn = GameObject.Find("StartGameBtn"); // Set Gameobjects
        TutorialBtn = GameObject.Find("TutorialBtn"); // Set Gameobjects
        AboutBtn = GameObject.Find("AboutBtn"); // Set Gameobjects
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Main.MenuState == 1) // If Camera fixed on Start Screen
        {
            if (AnimationPlayed == false) // if animation hasent been played yet
            {
                StartGameBtn.animation.Play("StartBtnIntro"); // play intro animaition
                AboutBtn.animation.Play("AboutBtnIntro"); // play intro animaition
                TutorialBtn.animation.Play("TutorialBtnIntro"); // play intro animaition
                AnimationPlayed = true;  // animation has been played
            }
        }

        if (Main.MenuState == 5) // If Camera is in transition
        {
            if (AnimationPlayed == true) // if animation has been played
            {
                StartGameBtn.animation.Play("StartBtnOutro"); // play outro animaition
                AboutBtn.animation.Play("AboutBtnOutro"); // play outro animaition
                TutorialBtn.animation.Play("TutorialBtnOutro"); // play outro animaition
                AnimationPlayed = false; // animation has not been played
            }
        }
	}
}
