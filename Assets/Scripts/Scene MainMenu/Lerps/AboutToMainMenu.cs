﻿﻿using UnityEngine;
using System.Collections;

public class AboutToMainMenu : MonoBehaviour
{
	public Transform startPosition;
	public Transform endPosition;
	Vector3 startAngle;
	Vector3 endAngle;
	
	public GameObject Sound;
	public AudioClip MenuBack;
	public bool AudioPlayed = false;
	
	private float startTime;
	private float journeyLength;
	public static bool Lerp = false;
	public bool Lerping = false;
	
	private float distance = 9.9f;
	public bool buttonDrag = false;
	
	private Vector3 originalPosition;
	private Vector3 worldPosition;
	
	public bool Vibrated = false;
	public bool AnimationPlayed = false;
	
	Canvas aboutUs;
	
	
	void OnMouseDown()
	{
		originalPosition = transform.localPosition;
		worldPosition = Camera.main.WorldToScreenPoint(transform.position);
	}
	
	
	void OnMouseDrag()
	{
		buttonDrag = true;
		
		if (Lerping == false && Main.MenuState == 3)
		{
			Vector3 mousePosition = new Vector3(Input.mousePosition.x, worldPosition.y, distance);
			Vector3 ObjPosition = Camera.main.ScreenToWorldPoint(mousePosition);
			transform.position = ObjPosition;
		}
		
		if (transform.localPosition.x < (originalPosition.x - 0.3))
		{
			if (AudioPlayed == false)
			{
				Sound.audio.PlayOneShot(MenuBack);
				AudioPlayed = true;
			}
			
			if (Vibrated == false)
			{
				Handheld.Vibrate();
				Vibrated = true;
			}
			
			Lerp = true;
			//animation.Play("AboutMainMenuBtnOutro");
		}
	}
	
	
	void OnMouseUp()
	{
		transform.localPosition = originalPosition;
		buttonDrag = false;
	}
	
	
	public void LerpFunction()
	{
		float distCovered = (Time.time - startTime);
		float fracJourney = distCovered / journeyLength;
		Camera.main.transform.position = Vector3.Lerp(startPosition.position, endPosition.position, fracJourney);
		
		startAngle = new Vector3(
			Mathf.LerpAngle(startAngle.x, endAngle.x, (Main.lerpSpeed * Time.deltaTime)),
			Mathf.LerpAngle(startAngle.y, endAngle.y, (Main.lerpSpeed * Time.deltaTime)),
			Mathf.LerpAngle(startAngle.z, endAngle.z, (Main.lerpSpeed * Time.deltaTime)));
		
		Camera.main.transform.eulerAngles = startAngle;
		
		if (Camera.main.transform.eulerAngles.y > (endAngle.y - 0.3f) && Camera.main.transform.eulerAngles.y < endAngle.y) // revise this
		{
			Camera.main.transform.eulerAngles = new Vector3(endAngle.x, endAngle.y, endAngle.z);
			AudioPlayed = false;
			Vibrated = false;
			Lerp = false;
			Lerping = false;
			startAngle = new Vector3(startPosition.eulerAngles.x, startPosition.eulerAngles.y, startPosition.eulerAngles.z);
			Main.MenuState = 1;
		}
		else if (Camera.main.transform.eulerAngles.y < (endAngle.y + 0.3f) && Camera.main.transform.eulerAngles.y > endAngle.y)
		{
			Camera.main.transform.eulerAngles = new Vector3(endAngle.x, endAngle.y, endAngle.z);
			AudioPlayed = false;
			Vibrated = false;
			Lerp = false;
			Lerping = false;
			startAngle = new Vector3(startPosition.eulerAngles.x, startPosition.eulerAngles.y, startPosition.eulerAngles.z);
			Main.MenuState = 1;
			AnimationPlayed = false;
		}
	}
	
	void Start()
	{
		startTime = Time.time;
		journeyLength = Vector3.Distance(startPosition.position, endPosition.position);
		startAngle = new Vector3(startPosition.transform.eulerAngles.x, startPosition.transform.eulerAngles.y, startPosition.transform.eulerAngles.z);
		endAngle = new Vector3(endPosition.transform.eulerAngles.x, endPosition.transform.eulerAngles.y, endPosition.transform.eulerAngles.z);
		
		aboutUs = GameObject.Find("AboutUsCanvas").GetComponent<Canvas>();
		aboutUs.enabled = false;
	}
	
	
	void Update()
	{
		if (Lerp == true)
		{
			aboutUs.enabled = false;
			Lerping = true;
			LerpFunction();
		}
		
		if (Main.MenuState == 3)
		{
			if (AnimationPlayed == false)
			{
				//animation.Play("AboutMainMenuBtnIntro");
				AnimationPlayed = true;
			}
		}
	}
	
	
	public void MouseClick()
	{
		Lerp = true;
	}
}