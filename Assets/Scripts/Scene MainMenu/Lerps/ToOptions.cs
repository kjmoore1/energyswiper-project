﻿using UnityEngine;
using System.Collections;

public class ToOptions : MonoBehaviour
{
    public Transform startPosition;
    public Transform endPosition;
    Vector3 startAngle;
    Vector3 endAngle;

    public GameObject Sound;
    public AudioClip MenuSelect;
    public bool AudioPlayed = false;

    private float startTime;
    private float journeyLength;
    public static bool Lerp = false;
    public bool Lerping = false;

    private float distance = 9.9f;
    public bool buttonDrag = false;

    private Vector3 originalPosition;
    private Vector3 worldPosition;

    public bool Vibrated = false;


    void OnMouseDown()
    {
        originalPosition = transform.localPosition;
        worldPosition = Camera.main.WorldToScreenPoint(transform.position);
    }


    void OnMouseDrag()
    {
        buttonDrag = true;

        if (Lerping == false && Main.MenuState == 1)
        {
            Vector3 mousePosition = new Vector3(worldPosition.x, Input.mousePosition.y, distance);
            Vector3 ObjPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            transform.position = ObjPosition;
        }

        if (transform.localPosition.y < (originalPosition.y - 0.3))
        {
            if (AudioPlayed == false)
            {
                Sound.audio.PlayOneShot(MenuSelect);
                AudioPlayed = true;
            }

            if (Vibrated == false)
            {
                Handheld.Vibrate();
                Vibrated = true;
            }
            
            Lerp = true;
            Main.MenuState = 5;
        }
    }


    void OnMouseUp()
    {
        transform.localPosition = originalPosition;
        buttonDrag = false;
    }


    public void LerpFunction()
    {
        float distCovered = (Time.time - startTime);
        float fracJourney = distCovered / journeyLength;
        Camera.main.transform.position = Vector3.Lerp(startPosition.position, endPosition.position, fracJourney);

        startAngle = new Vector3(
            Mathf.LerpAngle(startAngle.x, endAngle.x, (Main.lerpSpeed * Time.deltaTime)),
            Mathf.LerpAngle(startAngle.y, endAngle.y, (Main.lerpSpeed * Time.deltaTime)),
            Mathf.LerpAngle(startAngle.z, endAngle.z, (Main.lerpSpeed * Time.deltaTime)));

        Camera.main.transform.eulerAngles = startAngle;

        if (Camera.main.transform.eulerAngles.y > (endAngle.y - 0.3f) && Camera.main.transform.eulerAngles.y < endAngle.y) // revise this
        {
            Camera.main.transform.eulerAngles = new Vector3(endAngle.x, endAngle.y, endAngle.z);
            AudioPlayed = false;
            Vibrated = false;
            Lerp = false;
            Lerping = false;
            startAngle = new Vector3(startPosition.eulerAngles.x, startPosition.eulerAngles.y, startPosition.eulerAngles.z);
            Main.MenuState = 4;
        }
        else if (Camera.main.transform.eulerAngles.y < (endAngle.y + 0.3f) && Camera.main.transform.eulerAngles.y > endAngle.y)
        {
            Camera.main.transform.eulerAngles = new Vector3(endAngle.x, endAngle.y, endAngle.z);
            AudioPlayed = false;
            Vibrated = false;
            Lerp = false;
            Lerping = false;
            startAngle = new Vector3(startPosition.eulerAngles.x, startPosition.eulerAngles.y, startPosition.eulerAngles.z);
            Main.MenuState = 4;
        }
    }


    void Start()
    {
        startTime = Time.time;
        journeyLength = Vector3.Distance(startPosition.position, endPosition.position);
        startAngle = new Vector3(startPosition.transform.eulerAngles.x, startPosition.transform.eulerAngles.y, startPosition.transform.eulerAngles.z);
        endAngle = new Vector3(endPosition.transform.eulerAngles.x, endPosition.transform.eulerAngles.y, endPosition.transform.eulerAngles.z);
    }


    void Update()
    {
        if (Lerp == true)
        {
            Lerping = true;
            LerpFunction();
        }
    }
}