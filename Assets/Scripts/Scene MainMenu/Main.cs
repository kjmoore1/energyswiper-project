﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour 
{
    private static Main sInstance = null;
    public static Main Instance // Standard Singleton
    {
        get
        {
            if (sInstance == null)
            {
                GameObject MainSingleton = new GameObject();
                sInstance = MainSingleton.AddComponent<Main>();
            }
            return sInstance;
        }
    }


    public static float lerpSpeed = 5; // Lerp Speed

    public static int MenuState = 1; // 1 = Start Page, 2 = Tutorial Page, 3 = About Page, 4 = Options Page, 5 = In Transition, 6 = StartGame


    void Awake()
    {
        if (sInstance == null) // if Dosen't exist
            sInstance = this; // this is the singleton
        else
            enabled = false; // if another instance exists disable this one
    }


	// Use this for initialization
	void Start () 
    {
        Time.timeScale = 1; // set timescale back to normal speed
	}
	

	// Update is called once per frame
	void Update () 
    {

	}


    void FixedUpdate()
    {

    }


    void LateUpdate()
    {

    }
}
