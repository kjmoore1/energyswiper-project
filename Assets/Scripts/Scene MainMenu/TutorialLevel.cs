﻿using UnityEngine;
using System.Collections;

public class TutorialLevel : MonoBehaviour 
{
    public Transform spawner;
    public GameObject BadEnergy;
    public GameObject GoodEnergy;

    public float Timer = 37f;
    public bool TimerCounting = false;

    public static bool paused = false;
    private bool showTexture = false;

    public bool stage1Spawned = false;
    public bool stage2Spawned = false;
    public bool stage3Spawned = false;
    public bool stage4Spawned = false;
    public bool stage5Spawned = false;


    void OnMouseDown()
    {
        TimerCounting = true;
    }


    void BadEnergySpawn()
    {
        Vector3 initialForce = new Vector3(0, Random.Range(40.0F, 60.0F), Random.Range(-8.0F, 8.0F));
        Vector3 initialTorque = new Vector3(Random.Range(-10.0F, 10.0F), Random.Range(-10.0F, 10.0F), Random.Range(-10.0F, 10.0F));

        GameObject CubeInstance;
        CubeInstance = Instantiate(BadEnergy, spawner.transform.position, spawner.rotation) as GameObject;
        CubeInstance.rigidbody.AddForce(initialForce * 10);
        CubeInstance.rigidbody.AddTorque(initialTorque * 10);
    }

    void GoodEnergySpawn()
    {
        Vector3 initialForce = new Vector3(0, Random.Range(40.0F, 60.0F), Random.Range(-8.0F, 8.0F));
        Vector3 initialTorque = new Vector3(Random.Range(-10.0F, 10.0F), Random.Range(-10.0F, 10.0F), Random.Range(-10.0F, 10.0F));

        GameObject CubeInstance;
        CubeInstance = Instantiate(GoodEnergy, spawner.transform.position, spawner.rotation) as GameObject;
        CubeInstance.rigidbody.AddForce(initialForce * 10);
        CubeInstance.rigidbody.AddTorque(initialTorque * 10);
    }


    void PauseGame()
    {
            paused = !paused;

        if (paused == true) // If the game is paused
        {
            Time.timeScale = 0; // Set timescale to 0
            paused = true;
            showTexture = true;
        }

        if (paused == false) // If the game isn't paused
        {
            Time.timeScale = 1; // Set timescale to 1
            paused = false;
            showTexture = false;
        }

        if (showTexture == true)
        {
            GameObject.Find("PauseGuiTexture").guiTexture.enabled = true;
        }
        else
        {
            GameObject.Find("PauseGuiTexture").guiTexture.enabled = false;
        }
}

	// Use this for initialization
	void Start () 
    {
	
	}
	

	// Update is called once per frame
	void Update () 
    {
        if (TimerCounting == true)
        {
            Timer -= Time.deltaTime;

            if (Timer < 35f && stage1Spawned == false)
            {
                GoodEnergySpawn();
                BadEnergySpawn();
                stage1Spawned = true;
            }

            if (Timer < 30f && stage2Spawned == false)
            {
                GoodEnergySpawn();
                BadEnergySpawn();
                GoodEnergySpawn();
                BadEnergySpawn();
                stage2Spawned = true;
            }

            if (Timer < 25f && stage3Spawned == false)
            {
                GoodEnergySpawn();
                BadEnergySpawn();
                GoodEnergySpawn();
                BadEnergySpawn();
                GoodEnergySpawn();
                BadEnergySpawn();
                stage3Spawned = true;
            }

            if (Timer < 20f && stage4Spawned == false)
            {
                GoodEnergySpawn();
                BadEnergySpawn();
                GoodEnergySpawn();
                BadEnergySpawn();
                GoodEnergySpawn();
                BadEnergySpawn();
                GoodEnergySpawn();
                BadEnergySpawn();
                stage4Spawned = true;
            }

            if (Timer < 15f && stage5Spawned == false)
            {
                GoodEnergySpawn();
                BadEnergySpawn();
                GoodEnergySpawn();
                BadEnergySpawn();
                GoodEnergySpawn();
                BadEnergySpawn();
                GoodEnergySpawn();
                BadEnergySpawn();
                stage5Spawned = true;
            }
        }


	}
}
