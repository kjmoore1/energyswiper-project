﻿using UnityEngine;
using System.Collections;

public class XAxisLerpTemplate : MonoBehaviour
{
    // Start and end position/angles
    public Transform startPosition;
    public Transform endPosition;
    Vector3 startAngle;
    Vector3 endAngle;

    // Lerp variables
    private float startTime;
    private float journeyLength;
    public bool Lerp = false;
    public bool Lerping = false;

    // Vectors
    private Vector3 originalPosition;
    private Vector3 worldPosition;

    // Other variables
    private float distance = 9.9f;
    public bool buttonDrag = false;


    void OnMouseDown() // When mouse clicked object
    {
        originalPosition = this.transform.localPosition; // local position of button on first frame on mouse down
        worldPosition = Camera.main.WorldToScreenPoint(this.transform.position); // world position of button on first frame on mouse down
    }


    void OnMouseDrag() // When mouse held
    {
        buttonDrag = true; // button is being dragged

        if (Lerping == false) // if camera isnt lerping
        {
            Vector3 mousePosition = new Vector3(worldPosition.x, Input.mousePosition.y, distance); // mouse position on y axis but x axis is fixed
            Vector3 ObjPosition = Camera.main.ScreenToWorldPoint(mousePosition); // object position relative to camera
            this.transform.position = ObjPosition; // transform this object to that vector position
        }

        if (this.transform.localPosition.y > (originalPosition.y + 0.3)) // if this object moves certain distance from original position
        {
            Handheld.Vibrate(); // activate vibration
            Lerp = true; // activate lerp
        }
    }


    void OnMouseUp() // When click released
    {
        this.transform.localPosition = originalPosition; // return this to its original position
        buttonDrag = false; // button is not being dragged
    }


    public void LerpFunction()
    {
        float distCovered = (Time.time - startTime); // distance covered = current time - start time
        float fracJourney = distCovered / journeyLength; // fractions of journey are devided by the total length
        Camera.main.transform.position = Vector3.Lerp(startPosition.position, endPosition.position, fracJourney); // set camera position to next step in the journey

        startAngle = new Vector3(
            Mathf.LerpAngle(startAngle.x, endAngle.x, (Main.lerpSpeed * Time.deltaTime)),
            Mathf.LerpAngle(startAngle.y, endAngle.y, (Main.lerpSpeed * Time.deltaTime)),
            Mathf.LerpAngle(startAngle.z, endAngle.z, (Main.lerpSpeed * Time.deltaTime))); // calculate next angle of camera

        Camera.main.transform.eulerAngles = startAngle; // set camera to calculated angle

        if (Camera.main.transform.eulerAngles.x > (endAngle.x - 1f) && Camera.main.transform.eulerAngles.x < endAngle.x) // if camera is near end of lerp
        {
            Camera.main.transform.eulerAngles = new Vector3(endAngle.x, endAngle.y, endAngle.z); // transform camera angle to end angle
            Lerp = false;
            Lerping = false;
            startAngle = new Vector3(startPosition.eulerAngles.x, startPosition.eulerAngles.y, startPosition.eulerAngles.z); // set variable start angle back to normal
        }
        else if (Camera.main.transform.eulerAngles.x < (endAngle.x + 1f) && Camera.main.transform.eulerAngles.x > endAngle.x) // if camera is near end of lerp
        {
            Camera.main.transform.eulerAngles = new Vector3(endAngle.x, endAngle.y, endAngle.z); // transform camera angle to end angle
            Lerp = false;
            Lerping = false;
            startAngle = new Vector3(startPosition.eulerAngles.x, startPosition.eulerAngles.y, startPosition.eulerAngles.z); // set variable start angle back to normal
        }
    }


    void Start()
    {
        startTime = Time.time; // set start time to time applicaiton start
        journeyLength = Vector3.Distance(startPosition.position, endPosition.position); // find journey length
        startAngle = new Vector3(startPosition.transform.eulerAngles.x, startPosition.transform.eulerAngles.y, startPosition.transform.eulerAngles.z); // find start angle
        endAngle = new Vector3(endPosition.transform.eulerAngles.x, endPosition.transform.eulerAngles.y, endPosition.transform.eulerAngles.z); // find end angle
    }


    void Update()
    {
        if (Lerp == true)  // if lerp has been activated
        {
            Lerping = true; // camera is lerping
            LerpFunction();
        }
    }
}